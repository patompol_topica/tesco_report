<?php

use App\Http\Middleware\CheckAge;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('report/course/index', 'CourseReportController@index');
Route::get('report/course', 'CourseReportController@report');

//Route::get('report/make/{id}', 'ReportController@report');
//Route::get('report/remove/{id}', 'ReportController@remove');

