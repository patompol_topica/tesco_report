<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Course;
use App\Models\CourseClass;

use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\IOFactory;

class CourseReportController extends Controller {
    
    protected $spreadsheet;
    protected $sheet;
    
    public function index() {
        
        $courses = Course::all();
       
        $position = 0;
        
        foreach($courses as $course) {
            $position++;                
            echo "id: ".$course->id." position:".$position." name: ".$course->name."<br>";                    
        }
        
    }
    
    public function report() {
        
        ini_set('max_execution_time', 300);
        
        $this->spreadsheet = new Spreadsheet();
        $this->sheet = $this->spreadsheet->getActiveSheet();
        
        $offset = 400;
        $limit = 50;
        
        $courses = Course::skip($offset)
                ->take($limit)
                ->get();
        
        $report_index = 1;
        
        foreach($courses as $course_index => $course) {
            
            $users = $this->getUser($course->id);
            
            $report = $this->getReport($users);
            
            $report_index++;
            $this->export($report, $course->name, $report_index, $course_index);
            
            $report_index += count($report);
            
            //echo "total: ".count($report)." index ".$report_index."<br>";
            
            sleep(2);
            
        }
        
        $is_save = true;
        
        if($is_save) {
        
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="'."course".($offset+1)."~".($offset+$limit).'.xlsx"');

            $writer = IOFactory::createWriter($this->spreadsheet, 'Xlsx');
            $writer->save('php://output');
            
        } else {
            echo $this->spreadsheet->getActiveSheet()->getHighestRow();
        }
    }
    
    public function getUser($course_id) {
        return CourseClass::join('course', 'course.id', '=', 'class.course_id')
                ->select('is_finished', 'class.user_ids', 'class.updated_at')
                ->where('course_id', '=', $course_id)
                ->orderByRaw('is_finished DESC, class.user_id DESC, class.updated_at ASC')
                ->get();
    }
    
    public function getReport($users) {
        
        $report = new \Illuminate\Database\Eloquent\Collection();
        
        foreach($users as $user) {
            
            $user->user_ids = substr($user->user_ids, 1, -1);
            $ids = explode(',', $user->user_ids);
            
            foreach($ids as $id) {
                
                if($user->is_finished == 1) {
                    $user->is_finished = 'Pass';
                } else {
                    $user->is_finished = 'Not Pass';
                }
                
                $value = [
                    'id' => $id,
                    'is_finished' => $user->is_finished,
                    'year' => date('Y', strtotime($user->updated_at))
                ];
                
                array_add($report, $id, $value);
            }
            
        }
        
        //echo $report;
        
        return $report;
        
    } 
    
    public function export($report, $course_name, $report_index, $course_index) {
        
        $this->sheet->setCellValue('A1', 'Course Name');
        $this->sheet->setCellValue('B1', 'Year');
        $this->sheet->setCellValue('C1', 'Employee Id');
        $this->sheet->setCellValue('D1', 'Evaluate Result');
        
        $this->sheet->setCellValue('E1', 'Course Name');
        $this->sheet->setCellValue('F1', 'Start From');
        $this->sheet->setCellValue('E'.($course_index+2), $course_name);
        $this->sheet->setCellValue('F'.($course_index+2), $report_index);
        
        $index = $report_index;

        foreach ($report as $data) {

            $value = json_decode(json_encode($data));

            $this->sheet->setCellValue('A'.$index, $course_name);
            $this->sheet->setCellValue('B'.$index, $value->id);
            $this->sheet->setCellValue('C'.$index, $value->is_finished);
            $this->sheet->setCellValue('D'.$index, $value->year);

            $index++;
        }
        
    }
    
    
}
?>
