<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\StudentRecord;
use App\Models\Quiz;
use App\Models\User;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\IOFactory;
use DateTime;
use DateInterval;

class ReportController extends Controller {

  public function create() {
    return view('post.create');
  }

  public function store(Request $request) {
    $validateData = $request->validate([
        'title' => 'required|unique:posts|max:255',
        'body' => 'required',
    ]);
    
    $validated = $request->validated();
    
  }
  
  public function remove() {
      
      ini_set('max_execution_time', 180);
      
      $quizList = [7, 9, 10, 11,12,13,15,16,17,18,19,20,21,22,23,28,29,30,31,32,33,34,35,36,37,
          40,41,42,43,44,48,50,51,52,53,54,55,56,57,121,122,123,125,126,132,133,134,135,136,144,
          145,146,147,148,242,243,244,245,246,261,267,282,297,315,316,360,361];
      $quizLength = count($quizList);
      
      $test_index = 53; //40~53, 54!67
      
      //44
      
      $offset = 0;
      $limit = 3000;
      
      //for($i = 35 ; $i < 39 ; $i++) {
      for($i = 54 ; $i < 67 ; $i++) {
      //for($i = $test_index ; $i < ($test_index+1) ; $i++) {
          
          $quiz_id = $quizList[$i];
          
          $users = StudentRecord::select('user_id')
                  ->where('quiz_id', '=', $quiz_id)
                  ->groupby('user_id')
                  ->offset($offset)
                  ->limit($limit)
                  ->get();
          
          $userLength = count($users);
          
          foreach($users as $user) {
              
              $records = StudentRecord::select('is_success', 'id', 'score')
                      ->where('quiz_id', '=', $quiz_id)
                      ->where('user_id', '=', $user->user_id)
                      ->orderby('is_success', 'desc')
                      ->orderby('score', 'desc')
                      ->offset($offset)
                      ->limit($limit)
                      ->get();
              
              if(count($records) > 1) {
                  echo $user->user_id." ".$records."<br>";
                  $this->removeData($records);
                  
              }
              
          }
          
      }
      
  }
  
  private function removeData($records) {
      
      for($i = 1 ; $i < count($records) ; $i++) {
          echo 'remove '.$records[$i]->id.'<br>';
          
          $delete = StudentRecord::where('id', '=', $records[$i]->id)
                                  ->delete();
          
      }
      
  }
  
  public function report($quiz_position_id) {
      
      ini_set('max_execution_time', 180);
      
      $quizList = [7, 9, 10, 11,12,13,15,16,17,18,19,20,21,22,23,28,29,30,31,32,33,34,35,36,37,
          40,41,42,43,44,48,50,51,52,53,54,55,56,57,121,122,123,125,126,132,133,134,135,136,144,
          145,146,147,148,242,243,244,245,246,261,267,282,297,315,316,360,361];
      $quizLength = count($quizList);
      
      //$test_index = 0;
      
        $quiz_id = $quizList[$quiz_position_id];
        $quiz = Quiz::select('id', 'title')->where('id', '=', $quiz_id)->first();

        //echo "id: ".$quiz->id." position: ".$i." title: ".$quiz->title."<br>";

        $user = StudentRecord::join('user', 'user.id', '=', 'student_record.user_id')
                ->where('quiz_id', '=', $quiz_id)
                ->orderBy('user_id', 'ASC')
                ->get();

        $userLength = count($user);

        for($j = 0 ; $j < $userLength ; $j++) {

            $user[$j]->name_prefix = $user[$j]->title;

            $user[$j]->ac_name_prefix = $user[$j]->title_thai;
            $user[$j]->ac_name = $user[$j]->name_thai;

            $user[$j]->store_no = $user[$j]->st_format;
            $user[$j]->store_name = $user[$j]->st_thai;
            $user[$j]->work_level = $user[$j]->position_group;

            $startTime = $user[$j]->created_at;
            $completeTime = new DateTime($startTime);
            $completeTime->add(new DateInterval('PT' . $user[$j]->time_spent . 'M'));

            $user[$j]->start_training_date = date('Y-m-d',strtotime($startTime));
            $user[$j]->start_training_time = date('H:i:s',strtotime($startTime));
            $user[$j]->completed_training_date = $completeTime->format('Y-m-d');
            $user[$j]->completed_training_time = $completeTime->format('H:i:s');

            //$is_success = "Not Complete";
            if($user[$j]->is_success) {
                $user[$j]->is_success = 'Complete';
            } else {
                $user[$j]->is_success = 'Not Complete';
            }
            
            $user[$j]->percent = round(($user[$j]->score/$user[$j]->total_question)*100, 2)."%";
            
            //echo $user[$j]->score. " ".$user[$j]->total_question. " = ". $user[$j]->percent."<br>";
            
        }

        $is_save = true;

        if($is_save) {

            $spreadsheet = new Spreadsheet();
            $sheet = $spreadsheet->getActiveSheet();

            $sheet->setCellValue('A1', 'Employee ID');

            $sheet->setCellValue('B1', 'name prefix');
            $sheet->setCellValue('C1', 'Fisrt Name');
            $sheet->setCellValue('D1', 'Last Name');

            $sheet->setCellValue('E1', 'AC Name Prefix');
            $sheet->setCellValue('F1', 'AC Name');

            $sheet->setCellValue('G1', 'Position Title');
            $sheet->setCellValue('H1', 'Department');
            $sheet->setCellValue('I1', 'Store no.');
            $sheet->setCellValue('J1', 'Store Name');
            $sheet->setCellValue('K1', 'Work Level');
            $sheet->setCellValue('L1', 'Citizen ID');

            $sheet->setCellValue('M1', 'Start Date');
            $sheet->setCellValue('N1', 'Start Time');
            $sheet->setCellValue('O1', 'Completed Date');
            $sheet->setCellValue('P1', 'Completed Time');

            $sheet->setCellValue('Q1', 'Test Status');
            
            $sheet->setCellValue('R1', 'Score');
            $sheet->setCellValue('S1', 'Percent of score');
            $sheet->setCellValue('T1', 'Target ');

            for($i = 0 ; $i < count($user) ; $i++) {

                $index = $i+2;

                $sheet->setCellValue('A'.$index, $user[$i]->employee_id);

                $sheet->setCellValue('B'.$index, $user[$i]->name_prefix);
                $sheet->setCellValue('C'.$index, $user[$i]->first_name);
                $sheet->setCellValue('D'.$index, $user[$i]->last_name);

                $sheet->setCellValue('E'.$index, $user[$i]->ac_name_prefix);
                $sheet->setCellValue('F'.$index, $user[$i]->ac_name);

                $sheet->setCellValue('G'.$index, $user[$i]->position);
                $sheet->setCellValue('H'.$index, $user[$i]->department);
                $sheet->setCellValue('I'.$index, $user[$i]->store_no);
                $sheet->setCellValue('J'.$index, $user[$i]->store_name);
                $sheet->setCellValue('K'.$index, $user[$i]->work_level);
                $sheet->setCellValue('L'.$index, $user[$i]->citizen_id);

                $sheet->setCellValue('M'.$index, $user[$i]->start_training_date);
                $sheet->setCellValue('N'.$index, $user[$i]->start_training_time);
                $sheet->setCellValue('O'.$index, $user[$i]->completed_training_date);
                $sheet->setCellValue('P'.$index, $user[$i]->completed_training_time);

                $sheet->setCellValue('Q'.$index, $user[$i]->is_success);
                
                $sheet->setCellValue('R'.$index, $user[$i]->score);
                $sheet->setCellValue('S'.$index, $user[$i]->percent);
                $sheet->setCellValue('T'.$index, $user[$i]->target_percentage);

            }

            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="'.$quiz->title.'.xlsx"');

            $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
            $writer->save('php://output');

        } else {
            //echo "id: ".$quiz->id." title: ".$quiz->title. " ---> ".$userLength."<br>";
            echo $user;
            //echo $user->score." ".;
        }
          
      
  }

}

?>
