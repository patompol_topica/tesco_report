<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\User;

//use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class UserController extends Controller {

  public function show($id) {
      
      $user = User::where('id', '=', $id)->first();
      
      if(isset($user)) {
          //return $user->toArray();
          return view('user.profile')->with(['user' => $user]);
      } else {
          return view('user.profile')->with('name', "no user");          
      }
      
  }

  public function index() {
      
      //$users = User::all();
      //$users = App\User::find(1)->get();
      //$users = App\User::where('id', '>=', 2)->get();
      $users = User::groupBy('name')->get();
      /*foreach ($users as $user) {
          array_push($nameList, $user->id);
      }*/
      
      return $users->toArray();
      
      //return view('greeting')->with('name', implode(', ', $nameList));
      
  }
  
  public function add($name) {
      
      $user = User::where('name', '=', $name)->first();
      
      if(!isset($user)) {
          $user = new User();
          $user->name = $name;          
          echo 'register';
      } else {
          $user->updated_at = date('Y-m-d H:i:s');
          echo 'update '.$user->id;
      } 
      
      $user->save();
      
  }
  
  public function log() {
      Log::info("no user");
  }
  
}

?>
