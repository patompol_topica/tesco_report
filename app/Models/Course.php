<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use \Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    use SoftDeletes;
    
    public $timestamps = true;
    
    protected $table = 'course';
    //protected  

    
}
