<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use \Illuminate\Database\Eloquent\Model;

class Quiz extends Model
{
    use SoftDeletes;
    
    public $timestamps = true;
    
    protected $table = 'quiz';
    //protected  

    
}
