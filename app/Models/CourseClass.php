<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use \Illuminate\Database\Eloquent\Model;

class CourseClass extends Model
{
    use SoftDeletes;
    
    public $timestamps = true;
    
    protected $table = 'class';
    //protected  

    
}
