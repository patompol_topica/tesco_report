<?php

namespace App\Models;

//use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
//use Illuminate\Foundation\Auth\User as Authenticatable;
use \Illuminate\Database\Eloquent\Model;

class User extends Model
{
    //use Notifiable;
    use SoftDeletes;
    
    public $timestamps = true;
    
    protected $table = 'user';
    //protected  
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    //protected $fillable = [
      //  'name', 'email', 'password',
    //];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    //protected $hidden = [
      //  'password', 'remember_token',
    //];
    
}
